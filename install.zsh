# Adds a bootstrapping for our dotfiles zsh configs
echo "Adding initialization for Zsh dotfiles to the .zshrc"
echo "\n\n# Bootstrap the DOTDIR zsh init file" >> $HOME/.zshrc
echo '. $HOME/.dotfiles/init.zsh' >> $HOME/.zshrc

