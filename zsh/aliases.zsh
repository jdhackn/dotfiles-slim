# System
alias rzsh="source ${HOME}/.zshrc > /dev/null && echo 'Zsh reloaded!'"
alias l="ls -lh"
alias ll="ls -lA"
alias cl="clear"
alias running='ps aux | grep'
alias top='htop'

# Vim
alias v="vim"

# Git
alias gs="git status"
alias gp="git pull"
alias gpush="git push"
alias gal="git add -A && echo '[Git] Added all to staging...'"
alias gc="git commit"
alias co="git checkout"
alias gf="git flow"
alias gff="git flow feature"
alias gb="git branch"