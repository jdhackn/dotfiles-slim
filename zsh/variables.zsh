# System
export DOTDIR=$HOME/.dotfiles
export VIMDIR=$DOTDIR/vim

export VISUAL=vim
export EDITOR="$VISUAL"

