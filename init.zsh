export DOTDIR=$HOME/.dotfiles

. $DOTDIR/zsh/variables.zsh
. $DOTDIR/zsh/aliases.zsh
. $DOTDIR/zsh/functions.zsh
. $DOTDIR/zsh/hooks.zsh


# ASDF
# Initialize ASDF
. $HOME/.asdf/asdf.sh
# append completions to fpath
fpath=(${ASDF_DIR}/completions $fpath)
# initialise completions with ZSH's compinit
autoload -Uz compinit && compinit
